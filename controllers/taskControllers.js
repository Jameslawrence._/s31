const Task = require('../models/taskSchema');

exports.getSpecificTask = (taskId) => {
	//return Task.findById(taskId).then( task => { return task })
	const task = Task.findById(taskId);
	return task;
}

exports.getAllTask = () => {
	const task = Task.find({})
	return task;
}

exports.createTask = (reqBody) => {
	const newTask = new Task({
		name: reqBody.name
	})
	
	return Task.find({name: newTask.name}).then( result => {
		if(result.length === 0){
			newTask.save()
			return `${newTask.name} saved!`
		}else{
			return `Task already exist!`	
		}
	})
}


exports.updateStatusTask = (taskId, newStatus) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err)
			return false
		}
		
		result.status = newStatus.status
		return result.save().then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr)
				return false
			}else{
				return updatedTask
			}
		})
	})
}

exports.patchTask = (taskId, changes) => {
	return found = Task.findById(taskId).then((result, reject) => {
		if(reject){
			return console.log('not found')
		}

		Object.assign(result, changes)
		return result.save().then((changed, err) => {
			if(err){
				return console.log('not found')
			}else{
				return changed
			}
		})
	}).catch(reject => {
		return console.log('not found')
	})
}
