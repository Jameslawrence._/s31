const express = require('express');
const taskController = require('../controllers/taskControllers');

const router = express.Router();

router.get('/:id', (req, res) => {
	taskController.getSpecificTask(req.params.id).then( resultFromController => res.send(resultFromController))	
})


router.put('/:id/complete', (req, res) => {
	taskController.updateStatusTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})


router.post('/createTask', (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

router.get('/', (req, res) => {
	taskController.getAllTask().then(resultFromController => res.send(resultFromController))
})

router.patch('/patch-task/:id', (req, res) => {
	taskController.patchTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})


module.exports = router

